package com.appli.offerservices.service;

import java.util.List;
import java.util.Optional;

import com.appli.offerservices.model.Personne;

/**
 * @author Hardly
 *
 */
public interface IPersonneService {
	List<Personne> findAll();
	Personne findPersonneByName(String name);
	Optional<Personne> findById(long id);
}
