package com.appli.offerservices.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appli.offerservices.model.Personne;
import com.appli.offerservices.repository.PersonneRepository;

/**
 * @author Hardly
 *
 */

@Service
public class PersonneService implements IPersonneService {

	@Autowired
	PersonneRepository personneRepository;

	@Override
	public List<Personne> findAll() {
		return (List<Personne>) personneRepository.findAll();
	}

	@Override
	public Personne findPersonneByName(String name) {
		//return (List<Personne>) (personneRepository).findByNom(name);
		return personneRepository.findByNom(name);
	}

	@Override
	public Optional<Personne> findById(long id) {
		return personneRepository.findById(id);
	}

	public Personne save(Personne personne) {
		return personneRepository.save(personne);
	}

	public Personne updatePersonne(long id, Personne personne) {

		Optional<Personne> personneData = personneRepository.findById(id);
		if (personneData.isPresent()) {
			Personne personneRet = personneData.get();

			personneRet.setNom(personneRet.getNom());
			personneRet.setPrenom(personneRet.getPrenom());
			personneRet.setAge(personneRet.getAge());

			return personneRepository.save(personneRet);
		}
		return null;
	}

	public void deleteAll() throws Exception {
		try {
			personneRepository.deleteAll();
		} catch (Exception e) {
			throw new Exception();
		}
	}

	public void deletePersonne(long id) throws Exception {
		try {
			personneRepository.deleteById(id);
		} catch (Exception e) {
			throw new Exception();
		}
	}
}
