package com.appli.offerservices.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.appli.offerservices.model.Personne;

/**
 * @author Hardly
 *
 */

@Repository
public interface PersonneRepository extends CrudRepository<Personne, Long> {

	Personne findByNom(String name);
}
