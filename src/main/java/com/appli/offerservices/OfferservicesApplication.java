package com.appli.offerservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfferservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfferservicesApplication.class, args);
	}

}
