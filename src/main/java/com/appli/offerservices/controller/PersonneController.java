package com.appli.offerservices.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appli.offerservices.model.Personne;
import com.appli.offerservices.service.PersonneService;

/**
 * @author Hardly
 *
 */

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class PersonneController {

	@Autowired
	PersonneService personneService;

	@GetMapping("/personnes")
	public ResponseEntity<List<Personne>> getAllPersonnes() {
		try {
			List<Personne> personnes = new ArrayList<Personne>();

			personneService.findAll().forEach(personnes::add);

			if (personnes.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(personnes, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/personnes/{id}")
	public ResponseEntity<Personne> getPersonneById(@PathVariable("id") long id) {
		Optional<Personne> personne = personneService.findById(id);

		if (personne.isPresent()) {
			return new ResponseEntity<>(personne.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/personnes")
	public ResponseEntity<Personne> createPersonne(@RequestBody Personne personne) {
		try {
			Personne personneRet = personneService
					.save(new Personne(personne.getNom(), personne.getPrenom(), personne.getAge()));
			return new ResponseEntity<>(personneRet, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@PutMapping("/personnes/{id}")
	public ResponseEntity<Personne> updatePersonne(@PathVariable("id") long id, @RequestBody Personne personne) {

		Personne personneRet = personneService.updatePersonne(id, personne);
		if (personneRet != null) {
			return new ResponseEntity<>(personneRet, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/personnes/{id}")
	public ResponseEntity<HttpStatus> deletePersonne(@PathVariable("id") long id) {
		try {
			personneService.deletePersonne(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@DeleteMapping("/personnes")
	public ResponseEntity<HttpStatus> deleteAllPersonnes() {
		try {
			personneService.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
}
